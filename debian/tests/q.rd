<resource schema="it">
	<!-- this is maintained and developed on inputs/it/q.rd on Markus' box -->
	<STREAM id="make_test_table">
		<table id="t\nn" onDisk="True" adql="True" mixin="//scs#pgs-pos-index">
			<column name="ra" ucd="pos.eq.ra;meta.main"/>
			<column name="dec" ucd="pos.eq.dec;meta.main"/>
			<column name="recno" type="integer" required="True"
				ucd="meta.id;meta.main"/>
		</table>

		<data id="import_t\nn">
			<sources item="\nn"/>
			<embeddedGrammar><iterator>
				<code>
					n_items = int(self.sourceToken)
					for i in range(n_items):
						yield {
							"ra": i*0.5,
							"dec": (3-i)*0.25,
							"recno": i,}
				</code>
			</iterator></embeddedGrammar>
			<make table="t\nn"/>
		</data>
	</STREAM>

	<FEED source="make_test_table" nn="5"/>
	<FEED source="make_test_table" nn="25"/>

	<service id="cone" allowed="scs.xml">
		<scsCore queriedTable="t25">
			<FEED source="//scs#coreDescs"/>
		</scsCore>
	</service>

	<regSuite title="DaCHS regression">
		<regTest title="TAP query with pgsphere yields plausible result">
			<url parSet="TAP" QUERY="
				SELECT a.recno AS n1, b.recno AS n2
				FROM it.t5 AS a
				JOIN it.t25 AS b
				ON (distance(a.ra, a.dec, b.ra, b.dec)&lt;0.1)"
					>/tap/sync</url>
			<code>
				rows = self.getVOTableRows()
				self.assertEqual(len(rows), 5)
				for r in rows:
					self.assertEqual(r["n1"], r["n2"])
			</code>
		</regTest>

		<regTest title="SCS has a valid response">
			<url RA="5" DEC="-1.75" SR="0.8">/it/q/cone/scs.xml</url>
			<code>
				self.assertValidatesXSD()
				rows = self.getVOTableRows()
				self.assertEqual(len(rows), 3)
				self.assertEqual(
					set(rows[0].keys()), 
					{'_r', 'ra', 'dec', 'recno'})
			</code>
		</regTest>
	</regSuite>
</resource>
