"""
GAVO's VOTable python library.
"""

#c Copyright 2008-2022, the GAVO project <gavo@ari.uni-heidelberg.de>
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.


# Not checked by pyflakes: API file with gratuitous imports

from gavo.votable.coding import unravelArray

from gavo.votable.common import (VOTableError, VOTableParseError,
	BadVOTableLiteral, BadVOTableData, getLength)

# escapeX were part of this package's interface
from gavo.utils.stanxml import escapePCDATA, escapeAttrVal

from gavo.votable.model import VOTable as V, voTag

from gavo.votable.paramval import guessParamAttrsForValue, serializeToParam

from gavo.votable.votparse import parse, parseBytes, readRaw

from gavo.votable.simple import load, loads, save, makeDtype

from gavo.votable.tablewriter import (
	DelayedTable, OverflowElement, asBytes, write)

from gavo.votable.tapquery import ADQLTAPJob, ADQLSyncJob
from gavo.votable.tapquery import Error as TAPQueryError
