"""
DaCHS code; most of this is support code for running a data center within the
Virtual Observatory (VO), but some packages are interesting for VO clients
as well. 
"""
# This is a namespace package; do not put anything here
#

#c Copyright 2008-2022, the GAVO project <gavo@ari.uni-heidelberg.de>
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.


__import__('pkg_resources').declare_namespace(__name__)
