"""
A wrapper around pyparsing.  We need this because of the various
idiosyncrasies pyparsing has had over the years, and also because pyparsing
is not terribly well suited for the multi-grammar situation we have here.

Hence, whenever you use pyparsing in DaCHS: Use parsetricks instead of
pyparsing.

This is particularly important on older systems which may have pyparsings
older than 2.2.  There are rather significant changes from 2.2 to 2.4,
and we fall back to the built-in pyparsing (which is 2.4) if your pyparsing
is too old.
"""

#c Copyright 2008-2022, the GAVO project <gavo@ari.uni-heidelberg.de>
#c
#c This program is free software, covered by the GNU GPL.  See the
#c COPYING file in the source distribution.


# Not checked by pyflakes: (effectively) API file with gratuitous imports

import contextlib
import os
import threading

if True:
	from gavo.imp.pyparsing import *
else:
	import pyparsing
	if pyparsing.__version__<"2.4":
		from gavo.imp.pyparsing import *
	else:
		from pyparsing import *

####################### Pyparsing hacks
#
# (1) When building grammars, always do so using the pyparsingWhitechars
# context manager.  Building grammars is thread-safe, but different
# grammars here use different whitespace conventions, so without
# the c.m., you might get those messed up.
#
# (2) When parsing strings, *always* go through pyparseString(grammar,
# string) and fellow functions whenever your code could run from within
# the server (i.e., basically always outside of tests).
# pyparsing is not thread-safe, and thus we'll need to shoehorn some
# locking on top of it; I don't want to change the pyparsing methods
# themselves since they may be called very frequently.

ParserElement.enablePackrat()

_PYPARSE_LOCK = threading.RLock()

@contextlib.contextmanager
def pyparsingWhitechars(whiteChars):
	"""a context manager that serializes pyparsing grammar compilation
	and manages its whitespace chars.

	We need different whitespace definitions in some parts of DaCHS.
	(The default used to be " \\t" for a while, so this is what things
	get reset to).

	Since whitespace apparently can only be set globally for pyparsing,
	we provide this c.m.  Since it is possible that grammars will be
	compiled in threads (e.g., as a side effect of getRD), this is
	protected by a lock.  This, in turn, means that this can 
	potentially block for a fairly long time.

	Bottom line: When compiling pyparsing grammars, *always* set
	the whitespace chars explicitly, and do it through this c.m.
	"""
	_PYPARSE_LOCK.acquire()
	ParserElement.setDefaultWhitespaceChars(whiteChars)
	try:
		yield
	finally:
		ParserElement.setDefaultWhitespaceChars(" \t")
		_PYPARSE_LOCK.release()


def pyparseString(grammar, string, **kwargs):
	"""parses a string using a pyparsing grammar thread-safely.
	"""
	with _PYPARSE_LOCK:
		res = grammar.parseString(string, **kwargs)
		ParserElement.resetCache()
		return res


def pyparseTransform(grammar, string, **kwargs):
	"""calls grammar's transformString method thread-safely.
	"""
	with _PYPARSE_LOCK:
		return grammar.transformString(string, **kwargs)
